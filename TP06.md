Admin Système Linux, Minon Victor
===

# TP06 - Services réseau

## EXO1

**Adressage IP**


| Sous-réseau   | Nombre de machine | Adresse sous-réseau | Adresse broadcast | Adresse 1ère machine | Adresse dernière machine | Masque |
| ------------- | -------------     | -------------       | -------------     | -------------        | ------------- | ------------- |
| 1             | 38                | 172.16.0.0          | 172.16.0.63       | 172.16.0.1           | 172.16.0.62   | /27 |
| 2             | 33                | 172.16.0.64         | 172.16.0.127      | 172.16.0.65          | 172.16.0.126  | /27 |
| 3             | 52                | 172.16.0.128        | 172.16.0.191      | 172.16.0.129         | 172.16.0.190  | /27 |
| 4             | 35                | 172.16.0.192        | 172.16.0.255      | 172.16.0.193         | 172.16.0.254  | /27 |
| 5             | 34                | 172.16.1.0          | 172.16.1.63       | 172.16.1.1           | 172.16.1.62   | /27 | 
| 6             | 37                | 172.16.1.64         | 172.16.1.127      | 172.16.1.65          | 172.16.1.126  | /27 |
| 7             | 25                | 172.16.1.128        | 172.16.1.159      | 172.16.1.129         | 172.16.1.160  | /26 |

<br>

***

<br>

## EXO2
**Préparation de l'environnement**

Dans ce TP nous allons mettre en place un réseau rudimentaire constitué de seulement deux machines :
un serveur et un client :
- le serveur a une connexion Internet, notamment pour télécharger les paquets nécessaires à l’installation
des serveurs, et sert de passerelle au client ;
- les deux machines appartiennent à un réseau local;
- le client a accès à Internet uniquement via le serveur; il dispose d’une interface réseau qui recevra son
adresse IP du serveur DHCP. Dans un premier temps, cette interface sera activée mais débranchée.

1. VM éteintes, utilisez les outils de configuration de VirtualBox pour mettre en place l’environnement
décrit ci-dessus.

<br>

2. Démarrez le serveur et vérifiez que les interfaces réseau sont bien présentes. A quoi correspond l’interface
appelée lo ?
    >l'interface `lo` correspond a l'adresse de loopback, c'est a dire l'adresse `localhost`

<br>

3. Dans les versions récentes, Ubuntu installe d’office le paquet cloud-init lors de la configuration
du système. Ce paquet permet la configuration et le déploiement de machines dans le cloud via un
script au démarrage. Nous ne nous en servirons pas et sa présence interfère avec certains services (en
particulier le changement de nom d’hôte); par ailleurs, vos machines démarreront plus rapidement.
Désinstallez complètement ce paquet (il faudra penser à le faire également sur le client
ensuite.)

    ```bash
    (((echo 'datasource_list: [ None ]' | sudo -s tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg ) && sudo apt-get purge cloud-init -y ) && sudo rm -rf /etc/cloud/; sudo rm -rf /var/lib/cloud/ ) 1>/dev/null 2>&1 && echo "cloud-init removed" || echo "cloud-init not removed"
    ```

<br>

4. Les deux machines serveur et client se trouveront sur le domaine tpadmin.local. A l’aide de la
commande hostnamectl renommez le serveur (le changement doit persister après redémarrage,
donc cherchez les bonnes options dans le manuel!). On peut afficher le nom et le domaine d’une
machine avec les commandes hostname et/ou dnsdomainname ou en affichant le contenu du fichier /etc/hostname.
<i>Il se peut que l’ancien nom persiste dans le fichier /etc/hosts, associé à l’adresse IP 127.0.1.1 ; si
c’est le cas, modifiez ce fichier à la main.<br>
ISC DHCP Server utilise le temps UTC, qui diffère de l’heure locale française. C’est pourquoi vous
pourrez observer une différence entre la machine virtuelle et la machine hôte</i>

    ```bash
    sudo nano /etc/hosts
    sudo nano /etc/hostname
    sudo reboot -f
    hostname --fqdn
    ```
    >On saisi `serveur.tpadmin.local` ou `client.tpadmin.local` dans les deux fichier éditer par `nano`.<br>La derniere commande sert a tester si le changement a bien était pris en compte

<br>

***

<br>

## EXO3
**Installation du serveur DHCP**

Un serveur DHCP permet aux ordinateurs clients d’obtenir automatiquement une configuration réseau
(adresse IP, serveur DNS, passerelle par défaut...), pour une durée déterminée. <br>Ainsi, dans notre cas, l’inter-
faces réseau de client doit être configurée automatiquement par serveur. Le réseau local tpadmin.local
a pour adresse 192.168.100.0/24 (on aurait pu choisir une autre adresse; attention, 192.168.1.0/24 est
souvent réservée, par exemple par votre FAI).
1. Sur le serveur, installez le paquet isc-dhcp-server. La commande systemctl status isc-dhcp-server
devrait vous indiquer que le serveur n’a pas réussi à démarrer, ce qui est normal puisqu’il n’est pas
encore configuré (en particulier, il n’a pas encore d’adresses IP à distribuer).

    ```bash
    sudo apt-get install isc-dhcp-server
    systemctl status isc-dhcp-server
    ```

<br>

2. Un serveur DHCP a besoin d’une IP statique. Attribuez de manière permanente l’adresse IP 192.168.100.1
à l’interface réseau du réseau interne. Vérifiez que la configuration est correcte.

    ```yaml
    # This is the network config written by 'subiquity'
    network:
        version: 2
        renderer: networkd
        ethernets:
            ens160:
            dhcp4: true
            ens192:
                addresses:
                    - 192.168.100.1/24
    ```
    >On modifie le fichier `/etc/netplan/00-installer-config.yaml` pour correspondre a l'exemple ci-dessus

    <br>

3. La configuration du serveur DHCP se fait via le fichier /etc/dhcp/dhcpd.conf. Faites une sauvegarde
du fichier existant sous le nom dhcpd.conf.bak puis éditez le fichier dhcpd.conf.<br>
A quoi correspondent les deux premières lignes?<br>
<i>Les valeurs indiquées sur ces deux lignes sont faibles, afin que l’on puisse voir constituer quelques
logs durant ce TP. Dans un environnement de production, elles sont beaucoup plus élevées!</i>

    ```bash
    sudo cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.bak
    ```

    >On modifie ensuite le fichier `/etc/dhcp/dhcpd.conf` avec les informations ci dessous :

    ```bash
    default-lease-time 120;
    max-lease-time 600;
    authoritative; #DHCP officiel pour notre réseau
    option broadcast-address 192.168.100.255; #informe les clients de l'adresse de broadcast
    option domain-name "tpadmin.local"; #tous les hôtes qui se connectent au
    #réseau auront ce nom de domaine
    subnet 192.168.100.0 netmask 255.255.255.0 { #configuration du sous-réseau 192.168.100.0
    range 192.168.100.100 192.168.100.240; #pool d'adresses IP attribuables
    option routers 192.168.100.1; #le serveur sert de passerelle par défaut
    option domain-name-servers 192.168.100.1; #le serveur sert aussi de serveur DNS
    }
    ```
    >Les deux premieres lignes correspondent aux temps des baux DHCP

<br>

4. Éditez le fichier /etc/default/isc-dhcp-server afin de spécifier l’interface sur laquelle le serveur
doit écouter.

    ```bash
    sudo nano /etc/default/isc-dhcp-server
    ```
    >On modifie ensuite la ligne `INTERFACESv4` pour ajouter l'interface de notre réseau local `ens192` dans mon cas 

<br>

5. Validez votre fichier de configuration avec la commande dhcpd -t puis redémarrez le serveur DHCP
(avec la commande systemctl restart isc-dhcp-server) et vérifiez qu’il est actif.

    ```bash
    sudo dhcpd -t
    sudo systemctl restart isc-dhcp-server
    ```

<br>

6. Notre serveur DHCP est configuré! Passons désormais au client. Si vous avez suivi le sujet du TP 1,
le client a été créé en clonant la machine virtuelle du serveur. Par conséquent, son nom d’hôte est
toujours serveur. Vérifiez que la carte réseau du client est débranchée, puis démarrez le client (il est possible qu’il mette un certain temps à démarrer : ceci est dû à l’absence de connexion Internet).
Comme pour le serveur, désinstallez ensuite cloud-init, puis modifiez le nom de la machine (elle doit
s’appeler client.tpadmin.local).

    >Fais dans les deux premieres questions.

    <i> Pour empêcher la latence au démarrage si une machine n’a pas de connexion Internet, on peut ajouter
    la ligne optional: true dans son fichier de configuration /etc/netplan/00-installer-config.yaml<br>
    Il est possible que les commandes sudo prennent désormais un certain temps à s’exécuter sur le
    client. C’est parce que sudo utilise le fichier /etc/hosts qui contient l’ancien nom de la machine. Il
    faut modifier ce fichier à la main si vous rencontrer ce problème.</i>

<br>

7. La commande tail -f /var/log/syslog affiche de manière continue les dernières lignes du fichier
de log du système (dès qu’une nouvelle ligne est écrite à la fin du fichier, elle est affichée à l’écran).
Lancez cette commande sur le serveur, puis connectez la carte réseau du client et observez les logs
sur le serveur. Expliquez à quoi correspondent les messages DHCPDISCOVER, DHCPOFFER, DHCPREQUEST,
DHCPACK. Vérifiez que le client reçoit bien une adresse IP de la plage spécifiée précédemment.

    >- DHCPDISCOVER = découverte de l'adresse mac
    >- DHCPOFFER = offre une adresse réseau a l'adresse mac 
    >- DHCPREQUEST = reception de la demande
    >- DHCPACK = envoie de packet sur l'adresse réseau du client

<br>

8. Que contient le fichier /var/lib/dhcp/dhcpd.leases sur le serveur, et affiche la commande dhcp-lease-list ?
    
    >Le fichier contient les différentes connexion au serveur avec les logs d'allocations d'adresse. <br>
    La commande affiche ce fichier mis en forme.
<br>

9.  Vérifiez que les deux machines peuvent communiquer via leur adresse IP, à l’aide de la commande ping.
    
    ```bash
    ping 192.168.100.1
    ```
<br>

10.   Modifiez la configuration du serveur pour que l’interface réseau du client reçoive l’IP statique 192.168.100.20 :<br>
   
        `Serveur :`

        ```bash
        sudo nano /etc/dhcp/dhcpd.conf
        sudo reboot -f
        ```
        >On décommente les lignes et modifie les lignes précedent la ligne `fixed-address` avec les informations voulue (nom du client, adresse mac et adresse ip): 
    
        <i>Vérifiez que la nouvelle configuration a bien été appliquée sur le client (éventuellement, désactivez
        puis réactivez l’interface réseau pour forcer le renouvellement du bail DHCP, ou utilisez la commande
        dhclient -v).</i>

<br>

***

<br>

## EXO4

**Donner un accès à Internet au client**

A ce stade, le client est juste une machine sur notre réseau local, et n’a aucun accès à Internet. Pour
remédier à cette situation, on va se servir de la machine serveur (qui, elle, a un accès à Internet via son
autre carte réseau) comme d’une passerelle.

1. La première chose à faire est d’autoriser l’IP forwarding sur le serveur (désactivé par défaut, étant
donné que la plupart des utilisateurs n’en ont pas besoin). Pour cela, il suffit de décommenter la ligne
net.ipv4.ip_forward=1 dans le fichier /etc/sysctl.conf<br> Pour que les changements soient pris en
compte immédiatement, il faut saisir la commande sudo sysctl -p /etc/sysctl.conf.

    

    <i>Vérifiez avec la commande sysctl net.ipv4.ip_forward que la nouvelle valeur a bien été prise en
    compte. </i>

    >net.ipv4.ip_forward = 1

<br>

2. Ensuite, il faut autoriser la traduction d’adresse source (masquerading) en ajoutant la règle iptables
suivante :<br>
sudo iptables --table nat --append POSTROUTING --out-interface enp0s3 -j MASQUERADE
<br>

    Vérifiez à présent que vous arrivez à « pinguer » une adresse IP (par exemple 1.1.1.1) depuis le client.
    A ce stade, le client a désormais accès à Internet, mais il sera difficile de surfer : par exemple, il est même
    impossible de pinguer www.google.com. C’est parce que nous n’avons pas encore configuré de serveur DNS
    pour le client.

    `Serveur`
    ```bash
    sudo iptables --table nat --append POSTROUTING --out-interface ens160 -j MASQUERADE
    ```

    `Client`
    ```bash
    ping 1.1.1.1
    ```

<br>

***

<br>

## EXO5

**Installation du serveur DNS**

De la même façon qu’il est plus facile de retenir le nom d’un contact plutôt que son numéro de téléphone,
il est plus simple de mémoriser le nom d’un hôte sur un réseau (par exemple www.cpe.fr) plutôt que son
adresse IP (178.237.111.223).<br>

Dans les premiers réseaux, cette correspondance, appelée résolution de nom, se faisait via un fichier
nommé hosts (présent dans /etc sous Linux1). L’inconvénient de cette méthode est que lorsqu’un nom ou
une adresse IP change, il faut modifier les fichiers hosts de toutes les machines!<br>

Par conséquent, avec l’avènement des réseaux à grande échelle, ce système n’était plus viable, et une
autre solution, automatisée et centralisée cette fois, a été mise au point : DNS (Domain Name Server).<br>

Généralement, le serveur DNS utilisé est soit celui mis à disposition par le fournisseur d’accès à Internet,
soit un DNS public (comme celui de Google : 8.8.8.8, ou celui de Cloudflare : 1.1.1.1).<br>

Il est aussi très commun d’utiliser un serveur DNS privé, interne à l’organisation, afin de pouvoir résoudre
les noms des machines locales. Pour les requêtes extérieures, le serveur DNS privé passe alors la main à un
DNS externe.<br>

Il existe de nombreux serveurs DNS, mais le plus commun sous UNIX est Bind9 (Berkeley Internet
Name Daemon v.9).

1. Sur le serveur, commencez par installer bind9, puis assurez-vous que le service est bien actif.

    ```bash
    sudo apt-get install -y bind9
    service bind9 status
    ```
 <br>

2. A ce stade, Bind n’est pas configuré et ne fait donc pas grand chose. L’une des manières les simples
de le configurer est d’en faire un serveur cache : il ne fait rien à part mettre en cache les réponses de
serveurs externes à qui il transmet la requête de résolution de nom.<br>

    <i>Le binaire (= programme) installé avec le paquet bind9 ne s’appelle ni bind ni bind9 mais named...</i><br>

    Nous allons donc modifier son fichier de configuration : /etc/bind/named.conf.options. Dans ce
    fichier, décommentez la partie forwarders, et à la place de 0.0.0.0, renseignez les IP de DNS publics
    comme 1.1.1.1 et 8.8.8.8 (en terminant à chaque fois par un point virgule). Redémarrez le serveur
    bind9.


    <br>

3. Sur le client, retentez un ping sur www.google.fr. Cette fois ça devrait marcher! On valide ainsi la
configuration du DHCP effectuée précédemment, puisque c’est grâce à elle que le client a trouvé son
serveur DNS.

<br>

4. Sur le client, installez le navigateur en mode texte lynx et essayez de surfer sur fr.wikipedia.org
(bienvenue dans le passé...)

    ```bash
    sudo apt-get install -y lynx && lynx fr.wikipedia.org
    ```
    
<br>

***

<br>

## EXO6

**Configuration du serveur DNS pour la zone tpadmin.local**

L’intérêt d’un serveur DNS privé est principalement de pouvoir résoudre les noms des machines du réseau
local. Pour l’instant, il est impossible de pinguer par leur nom client depuis serveur et inversement.

1. Modifiez le fichier /etc/bind/named.conf.local et ajoutez les lignes suivantes :
zone "tpadmin.local" IN {
type master; // c'est un serveur maître
file "/etc/bind/db.tpadmin.local"; // lien vers le fichier de définition de zone
};

<br>

2. Créez une copie appelée db.tpadmin.local du fichier db.local. Ce fichier est un fichier configuration
typique de DNS, constitué d’enregistrements DNS (cf. cours). Dans le nouveau fichier, remplacez
toutes les références à localhost par tpadmin.local, et l’adresse 127.0.0.1 par l’adresse IP du
serveur.<br>
<i>La ligne root.tpadmin.local. indique en fait une adresse mail du responsable technique de cette
zone, où le symbole @ est remplacé par le premier point. Attention également à ne pas oublier
le point final, qui représente la racine DNS ; on ne le met pas dans les navigateurs, mais il est
indispensable dans les fichiers de configuration DNS!</i>
<br>

    <i>Le champ serial doit être incrémenté à chaque modification du fichier. Généralement, on lui donne
    pour valeur la date suivie d’un numéro sur deux chiffres, par exemple 2019031401.</i>

    ```bash
    sudo cp /etc/bind/db.local /etc/bind/db.tpadmin.local
    sudo nano etc/bind/db.tpadmin.local
    ```

    >On modifie le fichier pour ressembler a la configuration ci dessous  

    ```bash
    ; BIND data file for local loopback interface
    ;
    $TTL    604800
    @       IN      SOA     tpadmin.local. root.localhost. (
                        2021101802         ; Serial
                            604800         ; Refresh
                            86400         ; Retry
                            2419200         ; Expire
                            604800 )       ; Negative Cache TTL
    ;
    @       IN      NS      tpadmin.local.
    @       IN      A       192.168.100.1
    @       IN      AAAA    ::1
    ```
    
<br>

3. Maintenant que nous avons configuré notre fichier de zone, il reste à configurer le fichier de zone inverse,
qui permet de convertir une adresse IP en nom.
Commencez par rajouter les lignes suivantes à la fin du fichier named.conf.local :
zone "100.168.192.in-addr.arpa" {
type master;
file "/etc/bind/db.192.168.100";
};
Créez ensuite le fichier db.192.168.100 à partir du fichier db.127, et modifiez le de la même manière
que le fichier de zone. Sur la dernière ligne, faites correspondre l’adresse IP avec celle du serveur
(Attention, il y a un petit piège!).

    ```bash
    sudo cp /etc/bind/db.127 /etc/bind/db.192.168.100
    sudo nano /etc/bind/db.192.168.100
    ```

   >On modifie le fichier pour ressembler a la      configuration ci dessous

   ```bash
    ;
    ; BIND reverse data file for local loopback interface
    ;
    $TTL    604800
    @       IN      SOA     tpadmin.local. root.tpadmin.local. (
                                01         ; Serial
                            604800         ; Refresh
                            86400          ; Retry
                            2419200        ; Expire
                            604800 )       ; Negative Cache TTL
    ;
    @       IN      NS      tpadmin.local.
    1.      IN      PTR     tpadmin.local.
   ```

<br>

4. Utilisez les utilitaires named-checkconf et named-checkzone pour valider vos fichiers de configuration :
$ named-checkconf named.conf.local
$ named-checkzone tpadmin.local /etc/bind/db.tpadmin.local
$ named-checkzone 100.168.192.in-addr.arpa /etc/bind/db.192.168.100
Modifiez le fichier /etc/systemd/resolved.conf et décommentez la section DNS.

    ```bash
    cd /etc/bind
    named-checkconf named.conf.local
    named-checkzone tpadmin.local /etc/bind/db.tpadmin.local
    named-checkzone 100.168.192.in-addr.arpa /etc/bind/db.192.168.100
    sudo nano /etc/systemd/resolved.conf
    ```

    >On modifie le fichier pour ressembler a la      configuration ci dessous

    ```bash
    [Resolve]
    DNS=192.168.100.1
    #FallbackDNS=(DNS Secondaire)
    Domains=tpadmin.local
    #LLMNR=no
    #MulticastDNS=no
    #DNSSEC=no
    #DNSOverTLS=no
    #Cache=no-negative
    #DNSStubListener=yes
    #ReadEtcHosts=yes
    ```

<br>

5. Redémarrer le serveur Bind9. Vous devriez maintenant être en mesure de ”pinguer” les différentes
machines du réseau.  


    ```bash
    sudo reboot -f
    ping 192.168.100.20
    ```
